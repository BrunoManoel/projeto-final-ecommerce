package br.com.ecommerce.services;

import br.com.ecommerce.models.Cliente;
import br.com.ecommerce.repositories.ClienteRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

@SpringBootTest
public class ClienteServiceTeste {

    @MockBean
    private ClienteRepository clienteRepository;

    @Autowired
    private ClienteService clienteService;

    Cliente cliente;

    @BeforeEach
    private void setUp(){
        this.cliente = new Cliente();
        cliente.setId(1);
        cliente.setNome("Bruno Manoel");
        cliente.setCpf("331.764.288-20");
    }

    @Test
    public void testarBuscaDeClientePeloID(){
        Optional<Cliente> clienteOptional = Optional.of(cliente);
        Mockito.when(clienteRepository.findById(Mockito.anyInt())).thenReturn(clienteOptional);

        Cliente clienteObjeto = clienteService.buscarClientePeloId(4);

        Assertions.assertEquals(cliente, clienteObjeto );
    }
}