package br.com.ecommerce.services;

import br.com.ecommerce.models.Produto;
import br.com.ecommerce.repositories.ProdutoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class ProdutoServiceTeste {

    @MockBean
    private ProdutoRepository produtoRepository;

    @Autowired
    private ProdutoService produtoService;

    Produto produto;
    List<Produto> produtos;

    @BeforeEach
    private void setUp(){

        this.produto = new Produto();
        produto.setId(1);
        produto.setNome("Camisa Real Madrid");
        produto.setDescricao("Camisa do Real Madrid cor azul");
        produto.setCategoria("Esporte");
        produto.setPreco(109.99);
        produto.setQuantidade(10);

        this.produtos = Arrays.asList(produto);
    }

    @Test
    public void testarBuscaDeProdutoPeloID(){
        Optional<Produto> produtoOptional = Optional.of(produto);
        Mockito.when(produtoRepository.findById(Mockito.anyInt())).thenReturn(produtoOptional);
        Produto produtoObjeto = produtoService.listarProdutoPorId(1);
        Assertions.assertEquals(produto, produtoObjeto );
    }

    @Test
    public void testarSalvarProduto(){
        Mockito.when(produtoRepository.findAllById(Mockito.anyIterable())).thenReturn(produtos);
        Mockito.when(produtoRepository.save(Mockito.any(Produto.class))).then(objeto -> objeto.getArgument(0));
        Produto testeSalvarProduto = produtoService.salvarProduto(produto);
        Assertions.assertEquals("Camisa Real Madrid", testeSalvarProduto.getNome());
    }
}