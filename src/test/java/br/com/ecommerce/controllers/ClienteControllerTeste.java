package br.com.ecommerce.controllers;

import br.com.ecommerce.models.Cliente;
import br.com.ecommerce.services.ClienteService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

@WebMvcTest(ClienteController.class)
public class ClienteControllerTeste {

    @MockBean
    private ClienteService clienteService;

    @Autowired
    private MockMvc mockMvc;

    Cliente cliente;
    //Produto produto;
    //List<Produto> produtos;
    //CadastroDeLeadDTO cadastroDeLeadDTO;

    @BeforeEach
    private void setUp(){
        this.cliente = new Cliente();
        cliente.setId(1);
        cliente.setNome("Bruno Manoel");
        cliente.setCpf("331.764.288-20");

        //this.produto = new Produto();
        //produto.setId(1);
        //produto.setPreco(10.00);
        //produto.setNome("Guia do Mochileiro da Galaxia");
        //produto.setDescricao("um guia");

        //this.produtos = Arrays.asList(produto);

        //lead.setProdutos(produtos);

        //this.cadastroDeLeadDTO = new CadastroDeLeadDTO();
        //cadastroDeLeadDTO.setTelefone("8794821739");
        //cadastroDeLeadDTO.setNome("Xxablau");
        //cadastroDeLeadDTO.setEmail("email@email.com");
        //cadastroDeLeadDTO.setCpf("415.329.110-25");

        //List<IdProdutoDTO> idProdutoDTOS = new ArrayList<>();

        //cadastroDeLeadDTO.setProdutos(idProdutoDTOS);
    }

    //@Test
    //public void testarBuscarPorID() throws Exception {
    //    Mockito.when(leadService.buscarLeadPeloId(1)).thenReturn(lead);

    //    mockMvc.perform(MockMvcRequestBuilders.get("/leads/1")
    //            .contentType(MediaType.APPLICATION_JSON))
    //            .andExpect(MockMvcResultMatchers.status().isOk())
    //            .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));

    //}

    //@Test
    //public void testarBuscarPorIdQueNaoExiste() throws Exception{
    //    Mockito.when(leadService.buscarLeadPeloId(Mockito.anyInt())).thenThrow(RuntimeException.class);

    //    mockMvc.perform(MockMvcRequestBuilders.get("/leads/" + lead.getId())
    //            .contentType(MediaType.APPLICATION_JSON))
    //            .andExpect(MockMvcResultMatchers.status().isBadRequest());
    //}

    @Test
    public void testarLerTodosOsClientes() throws Exception {
        List<Cliente> relacaoClientes = new ArrayList<>();
        Mockito.when(clienteService.listarTodosCliente()).thenReturn(relacaoClientes);

        mockMvc.perform(MockMvcRequestBuilders.get("/clientes")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray());
    }

    @Test
    public void testarCadastrarCliente() throws Exception{
        Mockito.when(clienteService.cadastrarCliente(Mockito.any(Cliente.class))).thenReturn(cliente);
        ObjectMapper objectMapper = new ObjectMapper();

        String clienteJson = objectMapper.writeValueAsString(cliente);

        mockMvc.perform(MockMvcRequestBuilders.post("/clientes")
                .contentType(MediaType.APPLICATION_JSON).content(clienteJson))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    //@Test
    //public void testarValidacaoDeCadastroDeLead() throws Exception{
    //    Mockito.when(leadService.salvarLead(Mockito.any(CadastroDeLeadDTO.class))).thenReturn(lead);

    //    cadastroDeLeadDTO.setEmail("idygufy");
    //    ObjectMapper objectMapper = new ObjectMapper();
    //    String leadJson = objectMapper.writeValueAsString(cadastroDeLeadDTO);

    //    mockMvc.perform(MockMvcRequestBuilders.post("/leads")
    //            .contentType(MediaType.APPLICATION_JSON)
    //            .content(leadJson))
    //            .andExpect(MockMvcResultMatchers.status().isBadRequest());

    //    Mockito.verify(leadService, Mockito.times(0))
    //            .salvarLead(Mockito.any(CadastroDeLeadDTO.class));
    //}
}