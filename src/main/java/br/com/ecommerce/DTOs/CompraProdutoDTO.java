package br.com.ecommerce.DTOs;

public class CompraProdutoDTO {

    private String nomeProduto;

    private double preco;

    public CompraProdutoDTO() {
    }

    public CompraProdutoDTO(String nomeProduto, double preco) {
        this.nomeProduto = nomeProduto;
        this.preco = preco;
    }

    public String getNomeProduto() {
        return nomeProduto;
    }

    public void setNomeProduto(String nomeProduto) {
        this.nomeProduto = nomeProduto;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

}
