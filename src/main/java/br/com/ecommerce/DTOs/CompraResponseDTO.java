package br.com.ecommerce.DTOs;

import br.com.ecommerce.models.Compra;
import br.com.ecommerce.models.Produto;

import java.util.ArrayList;
import java.util.List;

public class CompraResponseDTO {

    private double valorTotal;

    private List<CompraProdutoDTO> produtos;

    private CompraClienteDTO clientes;

    public CompraResponseDTO(double valorTotal, List<CompraProdutoDTO> produtos, CompraClienteDTO clientes) {
        this.valorTotal = valorTotal;
        this.produtos = produtos;
        this.clientes = clientes;
    }

    public CompraResponseDTO() {
    }

    public double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(double valorTotal) {
        this.valorTotal = valorTotal;
    }


    public List<CompraProdutoDTO> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<CompraProdutoDTO> produtos) {
        this.produtos = produtos;
    }

    public CompraClienteDTO getClientes() {
        return clientes;
    }

    public void setClientes(CompraClienteDTO clientes) {
        this.clientes = clientes;
    }

    public void convertToDTO(Compra compra){

        this.produtos =  new ArrayList<>();
        this.clientes = new CompraClienteDTO();

        this.clientes.setCpf(compra.getCliente().getCpf());
        this.clientes.setNome(compra.getCliente().getNome());

        this.valorTotal = compra.getValorTotal();

        for (Produto item : compra.getProdutos()) {

            CompraProdutoDTO compraProdutoDTO = new CompraProdutoDTO();
            compraProdutoDTO.setNomeProduto(item.getNome());
            compraProdutoDTO.setPreco(item.getPreco());

            this.produtos.add(compraProdutoDTO);

        }
    }

}
