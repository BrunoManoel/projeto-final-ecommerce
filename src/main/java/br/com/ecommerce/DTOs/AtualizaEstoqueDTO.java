package br.com.ecommerce.DTOs;

public class AtualizaEstoqueDTO {

    private int quantidade;

    public AtualizaEstoqueDTO(int quantidade) {
        this.quantidade = quantidade;
    }

    public AtualizaEstoqueDTO(){}

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

}
