package br.com.ecommerce.DTOs;


import java.util.List;

public class CompraRequestDTO {

    private String cpf;
    private List<Integer> idProdutos;


    public CompraRequestDTO(){

    }

    public CompraRequestDTO(String cpf, List<Integer> idProdutos) {
        this.cpf = cpf;
        this.idProdutos = idProdutos;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public List<Integer> getIdProdutos() {
        return idProdutos;
    }

    public void setIdProdutos(List<Integer> idProdutos) {
        this.idProdutos = idProdutos;
    }
}
