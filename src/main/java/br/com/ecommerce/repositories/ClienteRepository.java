package br.com.ecommerce.repositories;

import br.com.ecommerce.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Integer> {

    Cliente findByCpf(String cpf);

}
