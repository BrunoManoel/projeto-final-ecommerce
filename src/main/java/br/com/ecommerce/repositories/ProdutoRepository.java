package br.com.ecommerce.repositories;

import br.com.ecommerce.models.Produto;
import org.springframework.data.repository.CrudRepository;

public interface ProdutoRepository extends CrudRepository<Produto, Integer> {
}
