package br.com.ecommerce.repositories;

import br.com.ecommerce.models.Compra;
import org.springframework.data.repository.CrudRepository;

public interface CompraRepository extends CrudRepository<Compra, Integer> {
}
