package br.com.ecommerce.models;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
public class Produto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotBlank
    @NotNull(message = "Nome não foi preenchida corretamente")
    private String nome;

    @NotBlank(message = "Categoria não foi preenchida corretamente")
    private String categoria;

    @NotBlank(message = "Descrição não foi preenchida corretamente")
    private String descricao;

    @Digits(integer = 6, fraction = 2, message = "Preço fora do padrão")
    @DecimalMin(value = "1.0", message = "Valor está fora do normal")
    private double preco;

    @Min(0)
    @NotNull(message = "Quantidade não foi preenchida corretamente")
    private int quantidade;

    public Produto(){}

    public Produto(int id, String nome, String categoria, String descricao, double preco, int quantidade) {
        this.id = id;
        this.nome = nome;
        this.categoria = categoria;
        this.descricao = descricao;
        this.preco = preco;
        this.quantidade = quantidade;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
}
