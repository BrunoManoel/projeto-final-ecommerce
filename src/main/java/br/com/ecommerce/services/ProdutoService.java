package br.com.ecommerce.services;

import br.com.ecommerce.DTOs.AtualizaEstoqueDTO;
import br.com.ecommerce.models.Produto;
import br.com.ecommerce.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProdutoService {

    @Autowired
    private ProdutoRepository produtoRepository;

    public Produto salvarProduto(Produto produto) {
        return produtoRepository.save(produto);
    }

    public Iterable<Produto> listarTodosOsProdutos(){
        return produtoRepository.findAll();
    }

    public Produto listarProdutoPorId(int id){

        Optional<Produto> produtoOptional = produtoRepository.findById(id);

        if(produtoOptional.isPresent()){
            Produto produto = produtoOptional.get();
            return  produto;
        }else{
            throw new RuntimeException("Produto não encontrado");
        }
    }

    public void deletarProduto(int id) {
        if(produtoRepository.existsById(id)){
            produtoRepository.deleteById(id);
        } else {
            throw new RuntimeException("Registro não existe");
        }
    }

    public Produto atualizarProduto(Produto produto, int id) {
        if(produtoRepository.existsById(id)){
            Produto produtoDB = listarProdutoPorId(id);
            produto.setId(produtoDB.getId());
            return produtoRepository.save(produto);
        } else {
            throw new RuntimeException("Registro não existe");
        }
    }

    public Produto atualizarEstoque(AtualizaEstoqueDTO atualizaEstoqueDTO, int id) {
        if(produtoRepository.existsById(id)){
            Produto produtoDB = listarProdutoPorId(id);

            int quantidadeAtual = produtoDB.getQuantidade();
            int novoValorQuantidade = quantidadeAtual + atualizaEstoqueDTO.getQuantidade();

            if(novoValorQuantidade < 0) throw new RuntimeException("Valor não consiste. Estoque negativo.");

            produtoDB.setQuantidade(novoValorQuantidade);

            return produtoRepository.save(produtoDB);
        } else {
            throw new RuntimeException("Registro não existe");
        }
    }
}
