package br.com.ecommerce.services;

import br.com.ecommerce.models.Cliente;
import br.com.ecommerce.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public ClienteService(){


    }

    public Cliente cadastrarCliente(Cliente cliente){

        return clienteRepository.save(cliente);

    }

    public Iterable<Cliente> listarTodosCliente(){
        return clienteRepository.findAll();
    }

    //Buscar Cliente
    public Cliente buscarClientePeloId(int id){
        Optional<Cliente> clienteOptional = clienteRepository.findById(id);

        if(clienteOptional.isPresent()){
            Cliente cliente = clienteOptional.get();
            return cliente;
        }else {
            throw new RuntimeException("O Cliente não foi encontrado!");
        }
    }

    // Atualizar Cliente
    public Cliente atualizarCliente(int id, Cliente cliente){
        Cliente clienteDB = buscarClientePeloId(id);

        cliente.setId(clienteDB.getId());
        return clienteRepository.save(cliente);
    }

    // Deletar Cliente
    public void deletarCliente(int id){
        if(clienteRepository.existsById(id)) {
            clienteRepository.deleteById(id);
        }else {
            throw new RuntimeException("O Cliente não foi encontrado!");
        }
    }

    public Cliente buscarPorCpf(String cpf){
        Cliente cliente;
        try {
            cliente = clienteRepository.findByCpf(cpf);
            return cliente;
        } catch (RuntimeException e){
            throw new RuntimeException("Cliente não encontrado" + e.getMessage());
        }

    }
}
