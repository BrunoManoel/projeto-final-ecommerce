package br.com.ecommerce.services;

import br.com.ecommerce.DTOs.CompraRequestDTO;
import br.com.ecommerce.DTOs.CompraResponseDTO;
import br.com.ecommerce.models.Cliente;
import br.com.ecommerce.models.Compra;
import br.com.ecommerce.models.Produto;
import br.com.ecommerce.repositories.ClienteRepository;
import br.com.ecommerce.repositories.CompraRepository;
import br.com.ecommerce.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class CompraService {

    @Autowired
    private CompraRepository compraRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private ProdutoRepository produtoRepository;

    @Autowired
    private ClienteService clienteService;


    public CompraService(){

    }

    //Cria uma compra
    public Compra salvarCompra(CompraRequestDTO compraRequestDTO){
        Cliente cliente = new Cliente();
        Iterable<Produto> listaProdutoDB = new ArrayList<>();
        Compra compra = new Compra();
        List<Produto> listaProdutosAtualizados = new ArrayList<>();
        double valorTotal = 0;

        cliente = clienteService.buscarPorCpf(compraRequestDTO.getCpf());
        compra.setCliente(cliente);
        List<Integer> idProdutosList = compraRequestDTO.getIdProdutos();
        listaProdutoDB = produtoRepository.findAllById(idProdutosList);

        for (Produto produto: listaProdutoDB) {
            Produto produtoAtualizado = new Produto();
            produtoAtualizado = produto;

            //Pega a quantidade de um dado produto
            int ocorrencias = Collections.frequency(idProdutosList, produto.getId());
            valorTotal += produto.getPreco() * ocorrencias;

            int quantidadeAtualizada = produto.getQuantidade() - ocorrencias;

            if(quantidadeAtualizada < 0) throw new RuntimeException("O produto " + produto.getNome() + " está esgotado");

            produtoAtualizado.setQuantidade(quantidadeAtualizada);
            listaProdutosAtualizados.add(produtoAtualizado);
        }

        compra.setProdutos(listaProdutosAtualizados);
        compra.setValorTotal(valorTotal);

        return compraRepository.save(compra);

    }

    public Iterable<CompraResponseDTO> lerTodasAsCompras(){
        List<CompraResponseDTO> listaDeCompra = new ArrayList<>();

        for (Compra item: compraRepository.findAll()) {
            CompraResponseDTO compraResponseDTO = new CompraResponseDTO();
            compraResponseDTO.convertToDTO(item);

            listaDeCompra.add(compraResponseDTO);
        }
        return listaDeCompra;
    }
}
