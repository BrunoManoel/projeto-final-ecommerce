package br.com.ecommerce.controllers;

import br.com.ecommerce.DTOs.CompraRequestDTO;
import br.com.ecommerce.DTOs.CompraResponseDTO;
import br.com.ecommerce.models.Compra;
import br.com.ecommerce.services.CompraService;
import br.com.ecommerce.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/compras")
public class CompraController {

    @Autowired
    private ProdutoService produtoService;

    @Autowired
    private CompraService compraService;


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Compra registraCompra(@RequestBody CompraRequestDTO compraRequestDTO){
        try{
            return compraService.salvarCompra(compraRequestDTO);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping
    public Iterable<CompraResponseDTO> lerCompras(){
        System.out.println("oi");
        return compraService.lerTodasAsCompras();
    }

}
