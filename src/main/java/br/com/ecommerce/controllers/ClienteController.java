package br.com.ecommerce.controllers;

import br.com.ecommerce.models.Cliente;
import br.com.ecommerce.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/clientes")
public class ClienteController {

    @Autowired
    ClienteService clienteService;

    //Cadastrar Cliente
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente registrarCliente(@RequestBody @Valid Cliente cliente){

        return clienteService.cadastrarCliente(cliente);
    }

    @GetMapping
    public Iterable<Cliente> listarTodosCliente(){
        return clienteService.listarTodosCliente();
    }

    //Consultar Cliente por ID
    @GetMapping("/{id}")
    public Cliente pesquisarPorId(@PathVariable(name = "id") int id){
        try{
            Cliente cliente = clienteService.buscarClientePeloId(id);
            return cliente;
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    //Atualização de Cliente
    @PutMapping("/{id}")
    public Cliente atualizarCliente(@RequestBody @Valid Cliente cliente, @PathVariable(name = "id") int id){
        try {
            Cliente clienteDB = clienteService.atualizarCliente(id, cliente);
            return clienteDB;
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    // Exclusão de Cliente
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarCliente(@PathVariable(name = "id") int id){
        try{
            clienteService.deletarCliente(id);
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

}
