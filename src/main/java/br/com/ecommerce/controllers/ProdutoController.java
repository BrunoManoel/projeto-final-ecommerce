package br.com.ecommerce.controllers;

import br.com.ecommerce.DTOs.AtualizaEstoqueDTO;
import br.com.ecommerce.models.Produto;
import br.com.ecommerce.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/produtos")
public class ProdutoController {

    @Autowired
    ProdutoService produtoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Produto salvarProduto(@RequestBody @Valid Produto produto){
        Produto objetoProduto = produtoService.salvarProduto(produto);
        return objetoProduto;
    }

    @GetMapping
    public Iterable<Produto> listarTodosOsProdutos(){
        return produtoService.listarTodosOsProdutos();
    }

    @GetMapping("/{id}")
    public Produto listarProdutoPorId(@PathVariable(name = "id") int id){
        try{
            Produto produto = produtoService.listarProdutoPorId(id);
            return produto;
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarProduto(@PathVariable(name = "id") int id){
        try {
            produtoService.deletarProduto(id);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Produto atualizarProduto(@RequestBody @Valid Produto produto, @PathVariable(name="id") int id){
        try{
            Produto produtoDB = produtoService.atualizarProduto(produto, id);
            return produtoDB;
        } catch(RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/atualiza_estoque/{id}")
    public Produto atualizaEstoque(@RequestBody AtualizaEstoqueDTO atualizaEstoqueDTO, @PathVariable(name="id") int id){
        try{
            return produtoService.atualizarEstoque(atualizaEstoqueDTO, id);
        } catch(RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

}
